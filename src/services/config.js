import axios from 'axios';

export const createRequest = credentials =>
  axios.create({
    timeout: 1000,
    baseURL: `https://any-cors.herokuapp.com/${credentials.host}`,
    auth: {
      username: credentials.email,
      password: credentials.password
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    }
  });
