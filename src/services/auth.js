import { createRequest } from './config';

export function authenticate(host, email, password) {
  const request = createRequest({ host, email, password });
  return request.get('/rest/api/3/myself');
}
