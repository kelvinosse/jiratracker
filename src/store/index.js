import Vue from 'vue';
import Vuex from 'vuex';

import { auth } from './modules/auth';
import { projects } from './modules/projects';
import { issues } from './modules/issues';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    projects,
    issues
  }
});
