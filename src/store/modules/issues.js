import { getProjectIssues, getProjectIssueLog } from '../../services/issues';
import { isNil } from 'ramda';

const state = {
  loading: false,
  error: '',
  issues: [],
  logs: []
};

const actions = {
  getIssues({ commit }, key) {
    commit('onImport');
    getProjectIssues(key).then(
      res => commit('onImportSuccess', res.data.issues),
      error => commit('onImportFailure', error.message)
    );
  },
  getIssueLog({ commit }, key) {
    commit('onLog');
    getProjectIssueLog(key).then(
      res => commit('onLogSuccess', res.data.values),
      error => commit('onLogFailure', error.message)
    );
  }
};

const mutations = {
  onImport(state) {
    state.loading = true;
  },
  onImportSuccess(state, issues) {
    state.issues = issues;
    state.loading = false;
  },
  onImportFailure(state, error) {
    state.loading = false;
    state.error = error;
  },
  onLog(state) {
    state.loading = true;
  },
  onLogSuccess(state, logs) {
    state.logs = logs.reduce((accum, current) => {
      accum.push({
        created: current.created,
        status: current.items.filter(({ field }) => field === 'status')[0]
      });
      return accum.filter(log => !isNil(log.status));
    }, []);
    state.loading = false;
  },
  onLogFailure(state, error) {
    state.loading = false;
    state.error = error;
  }
};

export const issues = {
  namespaced: true,
  state,
  actions,
  mutations
};
