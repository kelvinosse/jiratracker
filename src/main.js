import Vue from 'vue';
import Va from 'vue-atlas';
import 'vue-atlas/dist/vue-atlas.css';
import App from './App.vue';
import store from './store';
import router from './router';

Vue.config.productionTip = false;
Vue.use(Va, 'en');

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
